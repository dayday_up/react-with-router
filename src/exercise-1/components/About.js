import React from 'react';
import {Link} from 'react-router-dom';

const About = () => {
  return (
    <main className="home">
      <div>
        <p>this is profile and url is /about</p>
        <Link to="/">Home</Link>
      </div>
    </main>
  )
};

export default About;