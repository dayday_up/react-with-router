import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => {
  return(
    <nav className="nav-bar">
      <ul className="nav-list">
        <li className="nva-item"><Link to="/home">Home</Link></li>
        <li className="nva-item"><Link to="/profile">My Profile</Link></li>
        <li className="nva-item"><Link to="/about">About Me</Link></li>
        <li className="nva-item"><Link to="/products">Product</Link></li>
        <li className="nva-item"><Link to="/goods">Goods</Link></li>
      </ul>
    </nav>
    );
};
 export default Header;