import React from 'react';
import data from '../../exercise-2/mockups/data';
import {Route} from 'react-router';
import {BrowserRouter, Link} from 'react-router-dom';
const Products = () => {
  return (
    <main className="home">
      <div>
        <BrowserRouter>
          <Route path="/products" exact={true} component={Index} />
          <Route path="/products/:id" component={Child} />
        </BrowserRouter>
      </div>
    </main>
  )
};

const Child = (match) => {
  const product = data[match.match.params.id];
  return (
    <div>
      <h3>product details</h3>
      <p>{product.name}</p>
      <p>{product.id}</p>
      <p>{product["price"]}</p>
      <p>{product["quantity"]}</p>
      <p>{product["desc"]}</p>
    </div>
  );
};

const Index = () => {
  return (
    <div>
      {Object.keys(data).map((item, index) => {
        return (
          <Link key={index} to={`/products/${item}`}>{item}</Link>
        )
      })}
    </div>
  )

};

export default Products;